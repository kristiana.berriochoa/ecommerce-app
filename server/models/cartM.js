const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CartSchema = new Schema({ 
    adminID: {type: Schema.Types.ObjectId, ref: "admin"},
    items: [
        {
        product: {type: Schema.Types.ObjectId, ref: "products"},
        quantity: {type: Number, default: 1},
        }
    ] 
});

module.exports =  mongoose.model('cart', CartSchema);