/*
---- ADMIN CONTROLLER ----
const login = async (req, res) => {
    const { email , password } = req.body;
    if(!email || !password) return res.json({ ok: false, message: 'All fields are mandatory to login!' })
    try {
        const admin = await Admin.findOne({ email });
        if(!admin) return res.json({ ok: false, message: 'Please provide a valid email!' });
        const passwordsMatch = await bcrypt.compare(password, admin.password);
        if(passwordsMatch) {
            const adminID = admin._id;
            const token = jwt.sign(admin.toJSON(), process.env.SECRET, { expiresIn: 100080 }); //365 days
            res.json({ ok: true, message: 'Welcome back!', token, email, adminID }) 
        } else return res.json({ ok: false, message: 'Invalid password!'})
    }  
    catch(error) {
        res.json({ ok: false, error })
    }
};

---- CART CONTROLLER ----
class CartController {
    //GET ITEMS IN CART BY ADMIN ID
    async find (req, res){
        let { adminID } = req.params;
        try {
            const items = await Cart.findOne({ adminID: adminID })
            .populate("items.product")
            res.send(items);
        }
        catch(error){
            res.send({ error });
        };
    }
    
    //ADD ITEM TO CART
    async add (req, res){
        let { adminID, product_id, quantity } = req.body;
        try {
            const foundCart = await Cart.findOne({ adminID: adminID })
            if(foundCart) {
                const product = foundCart.items.find(item => item.product == product_id)
                if(product){
                    await Cart.findOneAndUpdate(
                        { adminID: adminID, items: { $elemMatch: {product: product_id} }},
                        { $inc: {"items.$.quantity": quantity }}
                    )
                    res.send({ product })
                } else {
                    const newProduct = { quantity: quantity, product: product_id};
                    await Cart.findOneAndUpdate(
                        { adminID: adminID},
                        {$addToSet: { items: newProduct } }    
                    )
                    res.send({ newProduct });
                }
            } else {
                await Cart.create({ adminID: adminID, items: [{quantity: quantity, product: product_id}] });
                res.send({ Cart });
            }
        }
        catch(error){
            res.send({ error });
        };
    }

    //CLEAR CART
    async clear (req, res){
        let { adminID } = req.body;
        try {
            const clearedCart = await Cart.findOneAndUpdate(
                { adminID: adminID }, { $set: { items: [] }}
            );
            res.send({ clearedCart });
        }
        catch(error) {
            res.send({ error });
        };
    }

    //DELETE ITEM FROM CART
    async remove (req, res){
        let { adminID, product_id } = req.body;
        try {
            const updatedCart = await Cart.findOneAndUpdate(
                { adminID: adminID }, 
                {$pull: {items: {product: product_id}}},
                {new: true}
            ).populate("items.product")
            res.send({ updatedCart });
        }
        catch(error) {
            res.send({ error });
        };
    }
};

DELETE ITEM FROM CART
async remove (req, res) {
    console.log("Item deleted!");
    let { adminID, product_id } = req.body;
    try {
        const deleted = await Cart.deleteOne(
            { adminID: adminID }, {_id: product_id})
        res.send({ deleted });
    }
    catch(error) {
        res.send({ error });
    };
}
ANOTHER OPTION:
    try {
        const updatedCart = await Cart.findOneAndUpdate(
            { adminID: adminID }, 
            {$pull: {items: {product: product_id}}}, // Pull product from cart by ID
            {new: true} // Set new to true to return the updated doc
        ).populate("items.product")
        res.send({ updatedCart });
    }

ADD ITEM TO CART
async add (req, res){
    console.log("Item added to cart!");
    let { adminID, product_id, quantity } = req.body;
    try {
        const itemInCart = await Cart.findOne({ adminID: adminID, "items.product": product_id })
        if(itemInCart) {
            await Cart.findOneAndUpdate(
                {adminID: adminID, "items.product": product_id }, { $inc: {"items.$.quantity": quantity }}
        )
        res.send({ Cart })
    } else {
        await Cart.create({ adminID: adminID });
        //const newProduct = { quantity: quantity, product: product_id};
        await Cart.update(
            { adminID: adminID},
            //{$addToSet: { items: newProduct } }
            {$addToSet: { items: { product: {_id: product_id}, quantity: quantity } } }    
        )
        res.send({ newProduct });
    }
    catch(error) {
        res.send({ error });
    };
}
ANOTHER OPTION:
    const product = foundCart.items.find(item => item.product == product_id)
    if(product){
        await Cart.findOneAndUpdate(
            { adminID: adminID, items: { $elemMatch: {product: product_id} }}, // Check if elements match
            { $inc: {"items.$.quantity": quantity }} // Increase quantity
        )
        res.send({ product })
    }
    await Cart.create({ adminID: adminID, items: [{quantity: quantity, product: product_id}] });
        const newProduct = { quantity: quantity, product: product_id};
        await Cart.findOneAndUpdate(
            { adminID: adminID},
            {$addToSet: { items: newProduct } }    
        )

---- EMAIL CONTROLLER ----
// selecting mail service and authorizing with our credentials
const transport = nodemailer.createTransport({
// you need to enable the less secure option on your gmail account
// https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
	auth: {
		user: creds.user,
		pass: creds.password,
	}
});
const send_email = async (req,res) => {
	const { name , email, subject , message } = req.body;
	const default_subject = 'This is a default subject';
	const mailOptions = {
		to: creds.USER,
		subject: "New message from " + name,
		html: '<p>'+(subject || default_subject)+ '</p><p><pre>' + message + '</pre></p>'
	}
    try {
        const response = await transport.sendMail(mailOptions);
        console.log('=========================================> Email sent!!')
        return res.json({ on: true, message: 'Confirmation email sent!' });
    }
    catch(err) {
        return res.json({ ok: false, message: err });
    }
}

---- PAYMENT CONTROLLER ----
const sk_test = require("../stripe.js");
const stripe = require("stripe")(sk_test);

const charge = async (req, res) => {
    console.log(sk_test, req.body);
    try {
        let { status } = stripe.charges.create({
            amount: req.body.amount * 100,
            currency: "eur",
            description: "An example charge",
            source: req.body.token_id
        });
        res.json({ status });
    } catch(error) {
        res
        .status(500)
        .json({ status: 'error' })
        .end();
    }
};

module.exports = { charge };

---- CART MODEL ----
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CartSchema = new Schema({ 
    adminID: {type: Schema.Types.ObjectId, ref: "admin"},
    items: [
        {
        product: {type: Schema.Types.ObjectId, ref: "products"},
        quantity: {type: Number, default: 1},
        }
    ] 
});

module.exports =  mongoose.model('cart', CartSchema);

---- CART ROUTER ----
const express     = require('express') 
const router      = express.Router()
const controller = require('../controllers/cartC');

// =============== GET ITEMS IN CART BY ADMIN ID ===============
router.get('/:adminID', controller.find);

// =============== ADD ITEM TO CART ===============
router.post('/add', controller.add);

// =============== CLEAR CART ===============
router.post('/clear', controller.clear);

// =============== REMOVE ITEM FROM CART ===============
router.delete('/delete', controller.remove);

module.exports  = router;
*/