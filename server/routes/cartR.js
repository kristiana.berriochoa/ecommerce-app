const express = require('express') 
const router = express.Router()
const controller = require('../controllers/cartC');

// =============== GET All ITEMS IN CART ===============
router.get('/:adminID', controller.find);

// =============== ADD ITEM TO CART ===============
router.post('/add', controller.add);

// =============== CLEAR CART ===============
router.post('/clear', controller.clear);

// =============== REMOVE ITEM FROM CART ===============
router.delete('/delete', controller.remove);

module.exports  = router;