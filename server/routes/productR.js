const express = require('express')
const router = express.Router();
const controller = require('../controllers/productC');

// =============== FIND ALL PRODUCTS ===============
router.get('/', controller.findAll);

// =============== FIND ONE PRODUCT BY ID ===============
router.get('/product_id/:product_id', controller.findOne);

// =============== ADD NEW PRODUCT ===============
router.post("/add", controller.add);

// =============== UPDATE ONE PRODUCT ===============
router.post('/update', controller.update);

// =============== DELETE ONE PRODUCT BY ID ===============
router.delete("/delete/:product_id", controller.remove);

module.exports = router;