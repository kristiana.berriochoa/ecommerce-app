const express = require('express') 
const router = express.Router()
const controller = require('../controllers/paymentC');

// =============== PAY FOR ITEMS IN CART ===============
router.post('/charge', controller.charge);

module.exports = router;