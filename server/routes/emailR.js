const express = require('express') 
const router = express.Router()
const controller = require('../controllers/emailC');

// =============== SEND EMAIL TO ADMIN ===============
router.post('/send_email', controller.send_email);

module.exports = router;