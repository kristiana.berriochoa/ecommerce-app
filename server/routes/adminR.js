const express = require('express'); 
const router = express.Router();
const controller = require('../controllers/adminC');

// =============== FIND ALL ADMINS ===============
router.get('/', controller.find);

// =============== REGISTER NEW ADMIN ===============
router.post('/register', controller.register);

// =============== LOGIN ADMIN BY ID ===============
router.post('/login', controller.login);

// =============== VERIFY ADMIN BY TOKEN ===============
router.post('/verify_token', controller.verify_token);

module.exports = router;