// =============== REQUIRE AND RUN EXPRESS IN A SINGLE LINE =====================
const express = require('express');
const app = express();
const port = process.env.port || 4000;
require('dotenv').config();
// ================= ADD BODY PARSER ================
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// =============== MONGO CONNECTION, CHECK IF DB IS RUNNING =====================
const mongoose = require('mongoose');
async function connecting(){
    try {
        await mongoose.connect('mongodb://127.0.0.1/cupcakeDb', { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true })
        console.log('Connected to DB')
    } catch ( error ) {
        console.log('Error: Appears your DB is not running, please start it again!');
    }
}
connecting() // temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true); // end connection to mongo and checking if DB is running
// ==================== CORS ========================
const cors = require('cors'); // to send request from different url
app.use(cors()); // to enable cors for any requests
app.use(function(req, res, next) { // allowing requests from the front-end to our server with api calls
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
	next();
});
// =============== ROUTES ===========================
// app.get('/', (req, res) => {
//     res.send({ message: 'We did it!' });
// });
const path = require('path');


app.use('/admin', require('./routes/adminR'));
app.use('/products', require('./routes/productR'));
app.use('/email', require('./routes/emailR'));
app.use('/cart', require('./routes/cartR'));
app.use('/payment', require('./routes/paymentR'));

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../user/build')));
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../user/build', 'index.html'));
});
// =============== START SERVER =====================
app.listen(port, () => 
    console.log(`server listening on port ${port}`
));