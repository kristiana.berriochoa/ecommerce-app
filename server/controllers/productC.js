const Products = require('../models/productM');

class ProductController {
    //FIND ALL PRODUCTS
    async findAll (req, res){
        try {
            const products = await Products.find({});
            res.send({ products });
        }
        catch(error){
            res.send({ error });
        };
    }

    //FIND ONE PRODUCT BY ID
    async findOne (req ,res){
        let { product_id } = req.params;
        try {
            const product = await Products.findOne({ _id: product_id });
            res.send({ product });
        }
        catch(error){
            res.send({ error });
        };
    }

    //ADD PRODUCT
    async add (req, res){
        let { name, image, description, price, stock } = req.body;
        try {
            const added = await Products.create({ 
                name: name,
                image: image,
                description: description,
                price: price,
                stock: stock,
            });
            res.send({ added });
        }
        catch(error){
            res.send({ error });
        };
    }

    //UPDATE PRODUCT
    async update (req, res){
        let { _id, newName, newImage, newDesc, newPrice, newStock } = req.body;
        try {
                const updated = await Products.updateOne(
                    { _id }, 
                    {$set: {
                        name: newName,
                        image: newImage,
                        description: newDesc,
                        price: newPrice,
                        stock: newStock, 
                    }} 
                );
                res.send({ updated });
        }
        catch(error){
            res.send({ error });
        };
    }

    //DELETE PRODUCT
    async remove (req, res){
        let { product_id } = req.params;
        try {
            const deleted = await Products.deleteOne({ _id: product_id });
            res.send({ deleted });
        }
        catch(error){
            res.send({ error });
        };
    }  
};

module.exports = new ProductController();