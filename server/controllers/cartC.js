const Cart = require('../models/cartM');

class CartController {
    //FIND ITEMS IN CART
    async find (req, res){
        let { adminID } = req.params;
        try {
            const items = await Cart.findOne({ adminID: adminID })
            .populate("items.product")
            res.send(items);
        }
        catch(error){
            res.send({ error });
        };
    }
    
    //ADD ITEM TO CART
    async add (req, res){
        let { adminID, product_id, quantity } = req.body;
        try {
            const foundCart = await Cart.findOne({ adminID: adminID })
            if(foundCart){
                const product = foundCart.items.find(item => item.product == product_id)
                if(product){
                    await Cart.findOneAndUpdate(
                        { adminID: adminID, items: {$elemMatch: {product: product_id}} },
                        { $inc: {"items.$.quantity": quantity} }
                    )
                    res.send({ product })
                } else {
                    const newProduct = { quantity: quantity, product: product_id };
                    await Cart.findOneAndUpdate(
                        { adminID: adminID },
                        { $addToSet: {items: newProduct} }    
                    )
                    res.send({ newProduct });
                }
            } else {
                const newCart = await Cart.create({ adminID: adminID, items: [{quantity: quantity, product: product_id}] });
                res.send({ newCart });
            }
        }
        catch(error){
            res.send({ error });
        };
    }

    //CLEAR CART
    async clear (req, res){
        let { adminID } = req.body;
        try {
            const clearedCart = await Cart.findOneAndUpdate(
                { adminID: adminID }, { $set: { items: [] }}
            );
            res.send({ clearedCart });
        }
        catch(error) {
            res.send({ error });
        };
    }

    //DELETE ITEM FROM CART
    async remove (req, res){
        let { adminID, product_id } = req.body;
        try {
            const updatedCart = await Cart.findOneAndUpdate( 
                { adminID: adminID },
                { $pull: {items: {product: product_id}} },
                { new: true }
            ).populate("items.product")
            res.send({ updatedCart });
        }
        catch(error) {
            res.send({ error });
        };
    }
};

module.exports = new CartController();