const Admin = require('../models/adminM');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const find = async (req, res) => {
    try {
        const admins = await Admin.find({});
        res.send(admins);
    }
    catch(error){
        res.send({ error });
    };
}

const register = async (req, res) => {
    const { email, password, password2 } = req.body;
    if(!email || !password || !password2) return res.send({ ok: false, message: 'All fields are mandatory to register!' });
    if(password !== password2) return res.send({ ok: false, message: 'Passwords must match!' });
    try {
        const admin = await Admin.findOne({ email })
        if(admin) return res.json({ ok: false, message: 'This email is already in use!' });
        const hash = await bcrypt.hash(password, saltRounds)
        const newAdmin = { email, password: hash }
            await Admin.create(newAdmin)
            res.json({ ok: true, message: 'Thank you for registering!' })
    } 
    catch(error) {
        res.json({ ok: false, error })
    }   
};

const login = async (req, res) => {
    const { email , password } = req.body;
    if(!email || !password) return res.json({ ok: false, message: 'All fields are mandatory to login!' })
    try {
        const admin = await Admin.findOne({ email });
        if(!admin) return res.json({ ok: false, message: 'Please provide a valid email!' });
        const passwordsMatch = await bcrypt.compare(password, admin.password);
        if(passwordsMatch) {
            const adminID = admin._id;
            const token = jwt.sign(admin.toJSON(), process.env.SECRET, { expiresIn: 100080 });
            res.json({ ok: true, message: 'Welcome back!', token, email, adminID }) 
        } else return res.json({ ok: false, message: 'Invalid password!'})
    }  
    catch(error) {
        res.json({ ok: false, error })
    }
};

const verify_token = (req, res) => {
    console.log(req.headers.authorization)
    const token = req.headers.authorization;
        jwt.verify(token, process.env.SECRET, (err, success) => {
            err ? res.json({ ok: false, message: 'Something went wrong!' }) : res.json({ ok: true, success})
        });      
};

module.exports = { find, register, login, verify_token };