var jwt	= require('jsonwebtoken');
var config = require('../config');

// ======================MIDDLEWARE====================== \\

var isLoggedIn = (req, res, next) => {
	var token = req.headers.authorization
	if(token) {
	  	try {
			const decoded = jwt.verify(JSON.parse(token), config.secret); 
			req.token = token;
            req.admin = decoded;
            if(decoded) {
				console.log('We are in business!')
                return next();
            }
	  	} 
	  	catch(error) { 
		  	res.json({ error: error })
	  	}	 
	}
};

module.exports = isLoggedIn;
