## Application Description
MERN (MongoDB, Express, React.js, Node) stack eCommerce web application; users can sign-up and login (with authentification), complete CRUD (create, read, update, delete) operations on existing products, modify and remove items from a shopping cart, and conduct fully automated payments using Stripe.

## Purchase Products
Card number: 4242 4242 4242 4242\
Expiration date: any date\
CVC code: any code

## View Application
Deploying on Digital Ocean: http://142.93.166.114/