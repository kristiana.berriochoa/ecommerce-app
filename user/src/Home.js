import React from 'react'

const Home = () => {
    return <div className = "header">
        <h1 className = "title">When in Doubt, Bake!</h1>
    </div>
};

export default Home;