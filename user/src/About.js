import React from 'react';

const About = (props) => {
    return <div className = "about">
        <h1 className = "section-title">About Us</h1>
        <div className = "about-content">
            <img className = "about-picture" src = {require('./Images/Kitchen.jpg')} alt = "kitchen"/>
            <h3 className = "about-text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></h3>
        </div>
        <div style = {{textAlign: 'center'}}>
            <button className = "about-button" onClick = {() => props.history.goBack()}>Back</button>
        </div>
    </div>
};

export default About;