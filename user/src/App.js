import React , { useState, useEffect } from 'react';
import Axios from 'axios';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { URL } from './config';
import Navbar from './Navbar';
import Home from './Home';
import About from './About';
import Products from './components/Products';
import Product from './components/Product';
import AddProduct from './components/AddProduct';
import UpdateProduct from './components/UpdateProduct';
import DeleteProduct from './components/DeleteProduct';
import Register from './components/Register';
import Login from './components/Login';
import Account from './components/Account';
import Cart from './components/Cart';
import Checkout from './components/Checkout';
import Contact from './Contact';
import Footer from './Footer';
import './index.css';

function App(){

  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const token = JSON.parse(localStorage.getItem('token'))

  const verify_token = async () => {
    if(token === null) return setIsLoggedIn(false)
    try {
      Axios.defaults.headers.common['Authorization'] = token
      const response = await Axios.post(`${URL}/admin/verify_token`, {token})
      return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false)
    } 
    catch(error) {
      console.log(error)
    }
  }

  useEffect(() => {
    verify_token()
  }, [])

  const login = (token) => {
    console.log("token ===>")
    localStorage.setItem('token', JSON.stringify(token))
    setIsLoggedIn(true)
  }

  const logout = () => {
    localStorage.removeItem("token");
    setIsLoggedIn(false);
  }

  return (
    <div className = "app">
      <Router>
        <Navbar isLoggedIn = {isLoggedIn} logout = {logout}/>
        <Route exact path = "/" component = {Home}/>
        <Route exact path = "/about" component = {About}/>
        <Route exact path = "/products" component = {Products}/>
        <Route exact path = "/product/:id" component = {Product}/>
        <Route exact path = "/products/add" component = {AddProduct}/>
        <Route exact path = "/products/update" component = {UpdateProduct}/>
        <Route exact path = "/products/delete/:id" component = {DeleteProduct}/>
       
        <Route exact path = "/register" component = {Register}/>
        <Route exact path = "/login" render = {props => <Login login = {login} {...props}/>}/>
        <Route exact path = "/account" render = {props => {return !isLoggedIn ? <Redirect to = {'/login'}/> : 
        <Account logout = {logout} {...props}/>}}/>

        <Route exact path = "/cart" component = {Cart}/>
        <Route exact path = "/payment" component = {Checkout}/>
        <Route exact path = "/contact" component = {Contact}/>
        <Footer/>
      </Router>
    </div>
  )
};

export default App;