import React from 'react';
import Axios from 'axios';
import { URL } from '../config';

class UpdateProduct extends React.Component {

    state = {
        name: " ",
        image: " ",
        description: " ",
        price: " ",
    }

    componentDidMount() {
        Axios.get(`${URL}/products/product` + this.props.match.params.id)
        .then(response => {
            this.setState({
                name: response.data.name,
                image: response.data.image,
                description: response.data.description,
                price: response.data.price,
            })
        })
        .catch(error => {
            console.log("Error!")
        })
    }

    handleName = (e) => { this.setState({ name : e.target.value }) }
    handleImage = (e) => { this.setState({ image : e.target.value }) }
    handleDesc = (e) => { this.setState({ description : e.target.value }) }
    handlePrice = (e) => { this.setState({ price : e.target.value }) }
    
    handleSubmit = async (e) => {
        e.preventDefault();
        const { name, image, description, price } = this.state;
        try {
            await Axios.post(`${URL}/products/update`, {
                _id: this.props.match.params.id,
                newName: name,
                newImage: image,
                newDesc: description,
                newPrice: price,
            })
        }
        catch(error){
            console.log(error);
        }
        window.location = "/products";
    }

    render() {
        return (
            <div className = "update">
                <h1 className = "section-title">Update a Cupcake:</h1>
                <div className = "form-container">
                    <form className = "product-form" onSubmit = {this.handleSubmit}>
                        <input className = "product-form-input" onChange = {this.handleName} name = "name" type = "text" placeholder = "New Cupcake Name"/>
                        <input className = "product-form-input" onChange = {this.handleImage} name = "image" type = "url" placeholder = "New Image (URL)"/>
                        <input className = "product-form-input" onChange = {this.handleDesc} name = "description" type = "text" placeholder = "New Description"/>
                        <input className = "product-form-input-2" onChange = {this.handlePrice} name = "price" type = "number" placeholder = "New Price"/>
                        <button className = "product-submit">Update</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default UpdateProduct;