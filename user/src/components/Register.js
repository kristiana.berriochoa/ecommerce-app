import React, { useState } from 'react';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';
import { URL } from '../config';

const Register = () => {

    const [form, setValues] = useState({
        email: " ",
        password: " ",
        password2: " ",
    });
    const [message, setMessage] = useState(" ")

    const handleChange = (e) => {
        setValues({ ...form, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const response = await Axios.post(`${URL}/admin/register`, {
                email: form.email,
                password: form.password,
                password2: form.password2,
            })
            setMessage(response.data.message);
            window.location = '/login';
        }
        catch(error) {
            console.log(error)
        }
    }

    return <div className = "register">
        <h1 className = "section-title">Make An Account:</h1>
        <h2 className = "login-new-user">Already a user?<NavLink className = "login-new-user" to = {`/login`}>Login here!</NavLink></h2>
        <div className = "form-container">
            <form className = "account-form" onSubmit = {handleSubmit} onChange = {handleChange}>
                <input className = "account-form-input" name = "email" type = "email" placeholder = "Email"/>
                <input className = "account-form-input" name = "password" type = "password" placeholder = "Password"/>
                <input className = "account-form-input" name = "password2" type = "password" placeholder = "Confirm password"/>
                <button className = "submit" type = "submit" label = "Register">Register</button>
                <div className = "message"><h4>{message}</h4></div>
            </form>
        </div>
       
    </div> 
};

export default Register;