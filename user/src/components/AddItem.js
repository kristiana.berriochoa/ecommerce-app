import React from 'react';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';
import { URL } from '../config';

class AddItem extends React.Component {

	state = {
		adminID: localStorage.getItem('adminID'),
		product_id: this.props.product_id,
		quantity: 1,
	}

	handleClick = () => {
		let { adminID, product_id, quantity } = this.state;
		this.add(adminID, product_id, quantity);
	}

	async add(){
		let { adminID, product_id, quantity } = this.state;
		try {
			const response = await Axios.post(`${URL}/cart/add`, {
				adminID: adminID,
				product_id: product_id, 
				quantity: quantity
            });
			console.log(response);		
		}
		catch(error) {
			console.log(error);
		}
		window.location = '/cart';
	}

	render() {
		const token = localStorage.getItem('token');
 		return (
			<div>
				{token ?
				<button className = "add-item-button" onClick = {this.handleClick}>Add to Cart</button>
				:
				<h2 className = "add-item-text">Want to purchase this cupcake?<NavLink className = "add-item-login" to = {`/login`}>Login here!</NavLink></h2>
				}
			</div>
		);
	}
}

export default AddItem;