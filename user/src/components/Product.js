import React from 'react';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';
import { URL } from '../config';
import AddItem from './AddItem'

class Product extends React.Component{
    
    state = {
        product: " ",
    };

    componentDidMount(){
        this.findOne();
    };

    async findOne(){
        try {
            const response = await Axios.get(`${URL}/products/product_id/` + this.props.match.params.id);
            this.setState({
                product: response.data.product
            });
            console.log(response)
        }
        catch(error){
            this.setState({ error })
        }
    }

    render() {
        const token = localStorage.getItem('token');
        let { product } = this.state;
        return (
            <div className = "product">
                <h1 className = "section-title">{product.name}</h1>
                <div className = "product-container">
                    <div className = "product-content">
                        <img className = "product-picture" src = {product.image} alt = "one-cupcake"/>
                        <div className = "product-miniColumn">
                            <h2 className = "product-text">{product.description}</h2>
                            <h2 className = "product-price">{product.price}€ / individual cupcake</h2>
                            {product.name ? 
                            <AddItem 
                                product_id = {product._id}
                                name = {product.name}
                                price = {product.price}/>
                            : null}
                        </div>
                    </div>
                    {token ? 
                        <div className = "delete-product">
                            <h3 className = "delete-product-text">Not a fan? Remove cupcake here.</h3>
                            <NavLink to = {`/products/delete/${product._id}`}>
                                <button className = "delete-product-button">X</button>
                            </NavLink>
                        </div>
                    : null}
                </div>
            </div>
        )
    }
};

export default Product;