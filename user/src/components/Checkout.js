import React from 'react';
import { Elements, StripeProvider } from 'react-stripe-elements';
import CheckoutForm from "./CheckoutForm.js";
import pk_test from "../pk.js";

class Checkout extends React.Component {

    render() {
        return (
            <StripeProvider apiKey = {pk_test}>
                <div className = "checkout">
                    <Elements>
                        <CheckoutForm/>
                    </Elements>
                </div>
            </StripeProvider>
        )
    }
};

export default Checkout;