import React from 'react';
import Axios from 'axios';
import { URL } from '../config';
import Total from './CartTotal'; 

class Cart extends React.Component {
	
	state = {
		items: [],
		copyItems: [],
		product_id: ' ',
		loading: true,
	};

	componentDidMount(){
		this.find();
	}

	async find(){
		try {
			const adminID = localStorage.getItem('adminID')
			const response = await Axios.get(`${URL}/cart/${adminID}`);
			this.setState({
				items: response.data.items,
				loading: false,
			});
			console.log(response);
		} 
		catch(error){
			this.setState({ loading: false, error });
		}
	};

	handleChange = (e, i) => {
		let inputValue = e.target.value;
		this.setState(prevState => {
			let items = [...prevState.items];
			items[i] = {...items[i], quantity: inputValue};
			return { items };
		});
	}
	
	async remove(product_id){
	 	try {
			const adminID = localStorage.getItem('adminID')
	 	 	const response = await Axios.delete(`${URL}/cart/delete`, {
				data: { adminID: adminID, product_id: product_id }
			});
 	 		this.setState({
				copyItems: response.data
			});
			window.location = '/cart';
	 	} 
	 	catch(error) {
		 	console.log(error);
		}
	};

	handleClear = async (e) => {
        e.preventDefault();
		try {
			const adminID = localStorage.getItem('adminID')
      		const response = await Axios.post(`${URL}/cart/clear`, {
				  adminID: adminID
			});
			console.log(response)
      		window.location = '/cart';
		}
        catch(error) {
            console.log(error);
        }
	};
	
	render() {
		let { items, loading } = this.state;
		return (
            <div className = "cart">
                <h1 className = "section-title">My Cart</h1>
				<div style = {cart.column}>
					{!loading ? 
					(items.map((product, idx) => {
						return (
							<div style = {cart.row} key = {product.product._id}>
								<img style = {cart.picture} src = {product.product.image} alt = "cupcake"/>
								<h3 style = {cart.text}>{product.product.name}</h3>
								<input style = {cart.input} type = "number" value = {product.quantity} onChange = {e => this.handleChange(e, idx)}/>
								<h3 style = {cart.text}>{product.quantity} * {product.product.price}€</h3>
								<button style = {cart.remove} onClick = {(e) => {this.remove(product.product._id);}}>X</button>
							</div>
						);
					})) 
					: 
					(<h2>Your cart is loading...</h2>)}
				</div>
				<div style = {cart.miniRow}>
					<h3 style = {{color: 'coral'}}>Not happy with your order? Start over!</h3>
					<button style = {cart.clear} onClick = {this.handleClear}>Clear Cart</button>
				</div>
				<Total items = {items}/>
        	</div>
        )
    }  
}

export default Cart;

const cart = {
    column: {
        display: 'flex',
		flexDirection: 'column',
	},
	row: {
        display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		alignItems: 'center',
		border: '1px solid orange',
        margin: '10px 50px',
    },
    picture: {
        margin: '20px 0px 20px 0px',
		width: '10%',
		borderRadius: '8px',
	},
	text: {
		color: 'darkRed'
	},
	input : {
		width: '3em',
		height: '4vh',
		fontFamily: 'Mukta Mahee',
		fontWeight: 'bold',
		color: 'darkRed',
	},
	remove: {
		width: '2em', 
		height: '5vh',
	},
	miniRow: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	clear: {
        width: '10%', 
        height: '6vh',
        margin: '0px 50px 0px 10px',
    }
}