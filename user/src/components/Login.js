import React , { useState } from 'react';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';
import { URL } from '../config';

const Login = (props) => {
    
    const [form, setValues] = useState({
        email: " ",
        password: " ",
    });
    const [message, setMessage] = useState(' ')

    const handleChange = (e) => {
        setValues({ ...form, [e.target.name]: e.target.value })
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await Axios.post(`${URL}/admin/login`, {
                email: form.email,
                password: form.password,
            })
            setMessage(response.data.message)
            if(response.data.ok) {
                localStorage.setItem('adminID', response.data.adminID);
                setTimeout( () => {
                    props.login(response.data.token)
                    props.history.push('/account')
                }, 2000)
            }
        }
        catch(error) {
            console.log(error)
        }
    };

    return <div className = "login">
        <h1 className = "section-title">Login:</h1>
        <h2 className = "login-new-user">Are you a new user?<NavLink className = "login-new-user" to = {`/register`}>Register here!</NavLink></h2>
        <div className = "form-container">
            <form className = "account-form" onSubmit = {handleSubmit} onChange = {handleChange}>
                <input className = "account-form-input" name = "email" type = "email" placeholder = "Email"/>
                <input className = "account-form-input" name = "password" type = "password" placeholder = "Password"/>
                <button className = "submit" type = "submit" label = "Login">Login</button>
                <div className = "message"><h4>{message}</h4></div>
            </form>
        </div>
    </div> 
};

export default Login;