import React from 'react';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';
import { URL } from '../config';

class DeleteProduct extends React.Component {

    handleRemove = async (e) => {
        e.preventDefault();
        try {
            const removal = await Axios.delete(`${URL}/products/delete/product_id` + this.props.match.params.id);
            console.log(removal)
        }
        catch(error){
            console.log(error);
        }
    }

    render() {
        return (
            <div className = "delete">
                <h1 className = "section-title">Thanks!</h1>
                <div style = {remove.content}>
                    <h2 style = {remove.text}>We appreciate your opinion.</h2>
                    <h2 style = {remove.text2}>If more than 10 other clients delete this cupcake, we will consider it a permanent change.</h2>
                    <NavLink to = {`/products/`}>
                        <button style = {remove.button} onSubmit = {this.handleRemove}>Back to Products</button>
                    </NavLink>
                </div>
            </div>
        )
    }
}

export default DeleteProduct;

const remove = {
    content: {
        padding: '0px 50px 50px',
        textAlign: 'center'
    },
    text: {
        textAlign: 'center',
        color: 'darkRed',
        fontSize: '30px',
    },
    text2: {
        textAlign: 'center',
        color: 'darkRed',
        fontSize: '30px',
        borderBottom: '1px solid pink',
        paddingBottom: '50px'
    },
    button: {
        width: '15%', 
        height: '6vh',
        margin: '50px',
    }
};