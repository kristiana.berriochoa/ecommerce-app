import React from 'react';
import Axios from 'axios';
import { URL } from '../config';

class AddProduct extends React.Component {

    state = {
        name: " ",
        image: " ",
        description: " ",
        price: " ",
        stock: " ",
    }

    handleName = (e) => { this.setState({ name : e.target.value }) }
    handleImage = (e) => { this.setState({ image : e.target.value }) }
    handleDesc = (e) => { this.setState({ description : e.target.value }) }
    handlePrice = (e) => { this.setState({ price : e.target.value }) }
    handleStock = (e) => { this.setState({ stock : e.target.value }) }
    
    handleSubmit = async (e) => {
        e.preventDefault();
        const { name, image, description, price, stock } = this.state;
        try {
            const response = await Axios.post(`${URL}/products/add`, {
                name: name,
                image: image,
                description: description,
                price: price,
                stock: stock
            });
            console.log(response);
            this.setState({
                name: " ",
                image: " ",
                description: " ",
                price: " ",
                stock: " ",
            })
        }
        catch(error){
            console.log(error);
        }
        window.location = "/products";
    }

    render() {
        return (
            <div className = "add">
                <h1 className = "section-title">Add a Cupcake:</h1>
                <div className = "form-container">
                    <form className = "product-form" onSubmit = {this.handleSubmit}>
                        <input className = "product-form-input" onChange = {this.handleName} name = "name" type = "text" placeholder = "Cupcake Name"/>
                        <input className = "product-form-input" onChange = {this.handleImage} name = "image" type = "url" placeholder = "Image (URL)"/>
                        <input className = "product-form-input" onChange = {this.handleDesc} name = "description" type = "text" placeholder = "Description"/>
                        <input className = "product-form-input-2" onChange = {this.handlePrice} name = "price" type = "number" placeholder = "Price"/>
                        <input className = "product-form-input-2" onChange = {this.handleStock} name = "stock" type = "number" placeholder = "Stock No. (Quantity)"/>
                        <button className = "product-submit">Add</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default AddProduct;