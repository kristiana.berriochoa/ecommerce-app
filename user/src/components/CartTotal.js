import React from 'react'; 
import { NavLink } from 'react-router-dom';

function Total(props) {
    let total = 0; 
        props.items.map((product, i) => {
            let price = Number(props.items[i].product.price);
            let quantity = Number(props.items[i].quantity);
            let indivCost = price.toFixed(2) * quantity.toFixed(2);
                total = total + indivCost;  
                return(total);  
        })
        console.log(total);
        localStorage.setItem('amount', total);

    return (
        <div>
            <h2 style = {cartTotal.container}>Total: {total.toFixed(2)}€</h2>
            <NavLink  to = {'/payment'}><button style = {cartTotal.buy}>Buy Cupcakes</button></NavLink>
        </div>
    )
};

export default Total;

const cartTotal = {
    container: {
        margin: '30px 50px 0px 50px',
        color: 'darkRed',
        fontSize: '30px'
    },
    buy: {
        width: '10%', 
        height: '6vh',
        margin: '0px 0px 50px 50px',
    }
}