import React from 'react';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';
import { URL } from '../config';

class Products extends React.Component{
    
    state = {
        products: [],
        loading: true,
    };

    componentDidMount(){
        this.findAll();
    };

    async findAll(){
        try {
            const response = await Axios.get(`${URL}/products/`);
            console.log(response)
            this.setState({
                products: response.data.products,
                loading: false,
            });
        }
        catch(error){
            this.setState({ error, loading: false })
        }
    }

    render() {
        let { products, loading } = this.state;
        return (
            <div className = "product-list">
                <h1 className = "section-title">Cupcakes Galore</h1>
                <div style = {cupcakes.container}>
                    {!loading ? (products.map((product, idx) => {
                        return (
                            <div style = {cupcakes.single} key = {idx}>
                                <NavLink style = {{textDecoration: 'none'}} to = {`/product/${product._id}`}>
                                    <h3 style = {cupcakes.names}>{product.name}</h3>
                                    <img style = {cupcakes.pictures} src = {product.image} alt = "cupcake"/>
                                </NavLink>
                            </div>
                        );
                    })) : (<h2>Cupcakes loading...</h2>)
                    }
                </div>
            </div>
        ) 
    }
}

export default Products;

const cupcakes = {
    container: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        padding: '0px 50px 25px',
    },
    single: {
        display: 'flex',
        flex: '1 1 0',
        flexDirection: 'column',
        textAlign: 'center',
        alignItems: 'center',
        padding: '25px 20px',
        borderTop: '1px solid orange',
    },
    names: {
        color: 'darkRed',
        fontSize: '25px',
        paddingBottom: '10px',
    },
    pictures: {
        width: '320px',
        borderRadius: '8px',
        border: '5px solid pink'
    }
}