import React from 'react';
import Axios from 'axios';
import { URL } from '../config';
import { CardNumberElement, CardExpiryElement, CardCVCElement, injectStripe, StripeProvider, Elements } from "react-stripe-elements";

class CheckoutForm extends React.Component {

    state = {
        errorMessage: " ",
        cardNumber: false,
        cardExpiry: false,
        cardCvc: false,
        adminID: localStorage.getItem('adminID'),
        amount: localStorage.getItem('amount'),
    };

    handleChange = ({ elementType, complete, error }) => {
        if(error) return this.setState({ errorMessage: error.code });
        return this.setState({ [elementType]: complete });
    };

    handleInputChange = (e) => this.setState({ [e.target.name]: e.target.value });

    handleSubmit = async (e) => {
        e.preventDefault();
        const { cardNumber, cardCvc, cardExpiry } = this.state;
        if(!cardNumber || !cardCvc || !cardExpiry) return alert("Please complete all fields!");
        const fullname = this.state.name + this.state.lastname;
        const { name, lastname, email, phone, pc, amount } = this.state;
        if(this.props.stripe) {
            const { token } = await this.props.stripe.createToken({ name: fullname, email });
            console.log('token ====>', token)
            const response = await Axios.post(`${URL}/payment/charge`, {
                token_id: token.id,
                amount,
                name,
                lastname,
                email,
                phone,
                pc
            });
            console.log('response ====>', response.data)
            response.data.status === "succeeded" ? alert("Payment successful!") : alert("Payment error!");
            const { adminID } = this.state;
            const clear = await Axios.post(`${URL}/cart/clear`, {
                adminID: adminID
            });
            console.log(clear)
            window.location = '/cart';
        } else {
            alert("Stripe.js has not loaded yet");
        }
    };

    render() {
        return (
            <div className = "checkout-section">
                <h1 className = "section-title">Complete Your Order</h1>
                <div className = "form-container">
                    <form className = "checkout-form" onSubmit = {this.handleSubmit}>
                        {/*************************** FIRST ROW ****************************/}
                        <div className = "form-row">
                            <label>Name
                            <input className = "form-input" required name = "name" type = "text" placeholder = "Jane" onChange = {this.handleInputChange}/>
                            </label>
                            <label>Lastname
                            <input className = "form-input" required name = "lastname" type = "text" placeholder = "Doe" onChange = {this.handleInputChange}/>
                            </label>
                        </div>
                        {/*************************** SECOND ROW ****************************/}
                        <div className = "form-row">
                            <label>Email
                            <input className = "form-input" required name = "email" type = "email" placeholder = "jane.doe@example.com" onChange = {this.handleInputChange}/>
                            </label>
                            <label>Phone number
                            <input className = "form-input" required name = "phone" type = "number" placeholder = "+34 816463723" onChange = {this.handleInputChange}/>
                            </label>
                        </div>
                        {/***************************** THIRD ROW *****************************/}
                        <div className = "form-row">
                            <label>Card Number
                            <CardNumberElement className = "form-input" onChange = {this.handleChange}/>
                            </label>
                            <label>Expiration Date
                            <CardExpiryElement onChange = {this.handleChange} />
                            </label>
                        </div>
                        {/*************************** FOURTH ROW ****************************/}
                        <div className = "form-row" >
                            <label>CVC
                            <CardCVCElement className = "form-input" onChange = {this.handleChange}/>
                            </label>
                            <label>Postal Code
                            <input name = "pc" type = "text" placeholder = "94115" className = "StripeElement" onChange = {this.handleChangeInput}/>
                            </label>
                        </div>
                        <div className = "error" role = "alert">{this.state.errorMessage}</div>
                        <button>Pay {this.state.amount}€</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default injectStripe(CheckoutForm);