import React from 'react';

const Account = (props) => {

	return <div className = "account">
	    <h1 className = "section-title">Welcome!</h1>
	    <h3 className = "account-text">We love hearing from our clients and could always use help to make our cupcakes better.</h3>
		<h3 className = "account-text">Be creative, we are open to your ideas!</h3>
		<div className = "account-container">
			<form className = "account-box">
				<h3>Suggestions?</h3>
				<p>Add a cupcake, we'll taste-test it.</p>
				<button className = "account-button" onClick = {() => {props.history.push('/products/add');}}>Add</button>
			</form>
			<form className = "account-box">
				<h3>Improvements?</h3>
				<p>Modify a cupcake, we'll consider it.</p>
				<button className = "account-button" onClick = {() => {props.history.push('/products/update');}}>Update</button>
			</form>
			<form className = "account-box">
				<h3>Not a fan?</h3>
				<p>Remove a cupcake here.</p>
				<button className = "account-button" onClick = {() => {props.history.push('/products/');}}>Delete</button>
			</form>
		</div>
	</div>
};

export default Account;