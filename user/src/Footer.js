import React from 'react';
import { NavLink } from 'react-router-dom';

const Footer = () => {
    return <footer className = "footer" >
        <p>Copyright © 2020</p>
        <p>|</p>
        <p>When in Doubt, Bake</p>
        <p>|</p>
        <p>All rights reserved</p>
        <p>|</p>
        <p>Have a question? <NavLink to = {"/contact"}>Contact us here!</NavLink></p>
    </footer>
};

export default Footer;