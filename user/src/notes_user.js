/*
----- NAVBAR -----
const Navbar = () => {
    <div>
        <ul onClick = {(e) => props.selectPage(e.target.textContent)}>
            <li>Homepage</li>
            <li>About</li>
            <li>Gallery</li>
            <li>Contact Us</li>
        </ul>
    </div>
}

class Navbar extends React.Component {
    render() {
        const token = localStorage.getItem('token');
        return (
            <div className = "navbar">
                <div className = "navbar-left">
                    <NavLink exact to = {"/"}>Home</NavLink>
                    <NavLink exact to = {"/about"}>About Us</NavLink>
                    <NavLink exact to = {"/products"}>Products</NavLink>
                </div>
                <div className = "navbar-right">
                    <NavLink exact to = {"/register"}>Register</NavLink>
                    <NavLink exact to = {"/account"}>Account</NavLink>
                    <NavLink exact to = {"/cart/"}>Cart</NavLink>
                    {token ? <NavLink exact to = "/login" onClick = {() => {this.props.history.push('/login'); this.props.logout()}}>Logout</NavLink>: null}
                </div>
            </div>
        )
    } 
};

----- APP -----
function App() {
    <Route exact path = "/about" render = {(props) => <About year = {2020} { ...props }/>}/>
    
    renderPages = (page) => {
        this.setState({ page }, () => {console.log(this.state)});
    };

    <Navbar selectPage = {this.renderPages}/>
    {this.state.page === "Home" ? <Home/> : 
    this.state.page === "About" ? <About/> :
    this.state.page === "Gallery" ? <Gallery/> : 
    this.state.page === "Contact Us" ? <Contact/> : 
    <Home/>}
}

class App extends React.Component {
    state = {
        isLoggedIn: true,
    }
        
    render () {
        return (
            <div className = "app">
                <Router>
                    <Navbar/>
                    <Route exact path = "/" component = {Home}/>
                    <Route exact path = "/about" component = {About}/>
                    <Route exact path = "/products" component = {Products}/>
                    <Route exact path = "/contact" render = {props => (!this.state.isLoggedIn ? 
                    <Redirect to = "/"/> : <Contact { ...props }/>)}/>
                    <Route exact path = "/login" component = {Login}/>
                    <Route exact path = "/product/:products" component = {Product}/>
                    <Route exact path = "/register" component = {Register}/>
                    <Route exact path = "/account" render = {props => {return !isLoggedIn ? <Redirect to = {'/login'}/>
                    : <Account logout = {logout} {...props}/>}}/>
                </Router>
                <Footer/>
            </div>
        )
    }
};

----- HOME -----
<button style = {home.button} onClick = {() => {props.history.push('/products')}}>Straight to the cupcakes</button>

----- ABOUT -----
const About = (props) => {
    return <div>
        <h1>About! (c) {props.year}</h1>
        <p onClick = {() => props.history.goBack()}>Go back</p>
    </div>
};
const about = {
    text: {
        textAlign: 'justify',
        textJustify: 'inter-word',
        color: 'gray',
        p: {
            display: 'block',
        }
    },
    button: {
        width: '5em', 
        height: '6vh',
        margin: '50px',
    }
};

----- CONTACT -----
const Contact = () => {
	return <h1>Contact us!</h1>
};
console.log("-- data --", name: e.target.elements.name.value, e.target.elements.email.value, e.target.elements.message.value)

----- REGISTER -----
this.setState({message: response.data.message});
    if(response.data.ok) await axios.post(`http://localhost:4000/email/confirmation`, {
        email: email,
        name: 'When in Doubt, Bake!',
        subject: "Welcome to When in Doubt, Bake!",
        header: "A message from us",
        message: "Thank you for registering with When in Doubt, Bake!"
    })
    return setTimeout(()=> {this.props.history.push('/account')}, 1000);

----- LOGIN -----
const Login = () => {
    return <h1>Login!</h1>
}

----- PRODUCTS -----
const Products = () => {
    let products = [
        {name: "Black shoes", id: 10},
        {name: "Black hat", id: 11}
    ];
        
    let renderProducts = products.map((product, index) => {
        return <NavLink key = {index} to = {`/product/${product.id}`}>
        <h1>{product.name}</h1>
        </NavLink>
    });
    
    return <div>
        {renderProducts}
    </div>
}

----- SINGLE PRODUCT -----
const Product = (props) => {
    return <h1>Product: {props.match.params.products}</h1>
}

----- ACCOUNT -----
<div style = {{textAlign: 'center'}}>
	<button style = {logout} onClick = {() => {props.history.push('/login'); props.logout()}}>Logout</button>
</div>
const logout: {
	width: '5em', 
	height: '6vh',
	marginBottom: '50px',
	backgroundColor: 'darkRed'
}

----- ADD PRODUCT -----
handleChange = (e) => {this.setState({ [e.target.name]: e.target.value })}

----- UPDATE PRODUCT -----
handleChange = (e) => {this.setState({ [e.target.name]: e.target.value })}

----- CART -----
async find() {
	try {
		const adminID = localStorage.getItem('adminID');
		const response = await Axios.get(`${URL}/cart/${adminID}`);
		this.setState({
			items: response.data.items,
			loading: false,
		});
		console.log(response)
	} 
	catch(error) {
		this.setState({ loading: false, error });
	}
};
    
handleRemove = async (e) => {
	e.preventDefault();
	try {
		const adminID = localStorage.getItem('adminID');
		const response = await Axios.delete(`http://localhost:4000/cart/delete`, {
			data: { adminID: adminID, product_id: product_id }
		})
		console.log(response);
		this.setState({
			cartItems: response.data.items
		});
		window.location = '/cart';
	}
    catch(error) {
        console.log(error);
    }
};

async clear() {
    try {
		const adminID = localStorage.getItem('adminID');
      	const response = await Axios.post(`http://localhost:4000/cart/clear`, {
          	adminID: adminID
		});
		console.log(response)
      	window.location = '/cart';
	} 
	catch(error) {
      	console.log(error);
    }
};
	
<NavLink to = {`/product/${product.product._id}`}>
    <img style = {cart.picture} src = {product.product.image} alt = "cupcake"/>
</NavLink>


----- ADD ITEM -----
	state = {
		adminID: localStorage.getItem('adminID'),
		product_id: this.props.product_id,
		quantity: 1,
	}

	handleClick = () => {
		let { adminID, product_id, quantity } = this.state;
		this.add(adminID, product_id, quantity);
	}

	async add() {
		let { adminID, product_id, quantity } = this.state;
		try {
			const response = await Axios.post(`${URL}/cart/add`, {
				adminID: adminID,
				product_id: product_id, 
				quantity: quantity
            });
			console.log(response);		
		}
		catch(error) {
			console.log(error);
		}
		window.location = '/cart';
	}

	render() {
 		return (
			<div>
				<button style = {add} onClick = {this.handleClick}>Add to Cart</button>
			</div>
		);
	}
*/