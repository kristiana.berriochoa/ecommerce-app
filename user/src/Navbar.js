import React from 'react'
import { NavLink } from 'react-router-dom';

class Navbar extends React.Component {

    render() {
        return (
            <div className = "navbar">
                <div className = "navbar-left">
                    <NavLink exact to = {"/"}>Home</NavLink>
                    <NavLink exact to = {"/about"}>About Us</NavLink>
                    <NavLink exact to = {"/products"}>Products</NavLink>
                </div>
                
                <div className = "navbar-right">
                    {
                    this.props.isLoggedIn ? 
                        <div className = "loggedIn">
                            <NavLink exact to = {"/account"}>Account</NavLink>
                            <p className = "logout" onClick = {() => {this.props.logout(); window.location = '/login'}}>Logout</p>
                            <NavLink exact to = {"/cart/"}>Cart</NavLink>
                        </div>
                        : 
                        <div>
                            <NavLink exact to = {"/register"}>Register</NavLink>
                            <NavLink exact to = {"/account"}>Account</NavLink>
                        </div>
                    }
                </div>
            </div>
        )
    } 
};

export default Navbar;