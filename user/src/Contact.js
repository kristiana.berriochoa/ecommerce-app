import React from 'react';
import Axios from 'axios';
import { URL } from './config';

//var subject = "Here is the message: "

class Contact extends React.Component {
  
  constructor(){
	super()
		this.state = {
			title: "What can we do for you?"
		}
	}

	handleSubmit(e){
    var that = this;
    var subject = "Here is the message: "
    e.preventDefault();
		const nameInput = e.target.elements.name;
		const emailInput = e.target.elements.email;
    	const messageInput = e.target.elements.message;
		const subjectInput = that.props.subject || subject;
		var data = { 
      		name: nameInput.value, 
    		email: emailInput.value, 
    		message: messageInput.value, 
      		subject: subjectInput,
    	}
		Axios.post(`${URL}/email/send_email`, data)
		.then((response) => {
			nameInput.value = " "
			emailInput.value = " "
      		messageInput.value = " "
			alert("Your message has been sent, thank you!")
		})
		.catch(function (error) {
			console.log(error);
		})
	}

	render() {
		return (
			<div className = "contact">
				<h1 className = "section-title">{this.props.title || this.state.title}</h1>
			  	<form onSubmit = {this.handleSubmit.bind(this)}>
					<input className = "contact-form-input" required = {true} type = 'text' placeholder = 'Subject' name = 'subject'/>
					<textArea required = {true} type = "text" placeholder = "Please, type your message" name = "message"
						style = {{
							border: '1px solid grey',
							width: "50%",
							paddingTop: '.5em',
							paddingLeft: "0.5em",
							display: "block",
							margin: "0 auto",
							minHeight: "20vh",
							marginBottom: "1em",
							fontFamily: 'Mukta Mahee',
						}}/>
					<input className = "contact-form-input" required = {true} type = "text" placeholder = "What is your name?" name = "name"/>
					<input className = "contact-form-input" required = {true} type = "email" placeholder = "What is your email?" name = "email"/>	
					<button type = "submit" label = "Send" style = {{margin: '1em', width: '7%', height: '6vh',}}>Submit</button>
        		</form>
			</div>
		)
	}
};

export default Contact;